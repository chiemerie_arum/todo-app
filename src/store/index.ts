import Vue from "vue";
import Vuex from "vuex";
import { StoreOptions } from "vuex";
import todoModule from "./modules/todos";
import { TodoState } from "./modules/todos/state";

export type RootState = {
  todo: TodoState
};


Vue.use(Vuex);

const store: StoreOptions<RootState> = {
  modules: {
    todo: todoModule
  },
};
export default new Vuex.Store(store);
