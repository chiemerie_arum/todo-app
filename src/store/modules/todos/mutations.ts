import { TodoState, Todo } from "./state";
import { MutationTree } from "vuex";

const todoMutations: MutationTree<TodoState> = {
  addTodo(state: TodoState, todo: Todo) {
    state.todos.push(todo);
  },

  setTodos(state: TodoState, todos: Array<Todo>) {
    state.todos = todos;
  },
};

export default todoMutations;