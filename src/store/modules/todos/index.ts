import { Module } from 'vuex';

import { RootState } from '../..';

import todoActions from './actions';
import todoGetters from './getters';
import todoMutations from './mutations';
import todoState, { TodoState as TodoStateType } from './state';

const todoModule: Module<TodoState, RootState> = {
  namespaced: true,
  actions: todoActions,
  getters: todoGetters,
  mutations: todoMutations,
  state: todoState,

};

export type TodoState = TodoStateType;
export default todoModule;
