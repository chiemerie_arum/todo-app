export type Todo = {
  id: number;
  title: string;
  date: number;
  done: boolean;
}

export type TodoState = {
  todos: Array<Todo>;
  inputText: string;
  expanded: boolean;
}

const todo: TodoState = {
  todos: [],
  inputText: "",
  expanded: false,
};

export default todo;