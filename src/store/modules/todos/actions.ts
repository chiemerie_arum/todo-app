import TodoService from "@/utils/TodoService";
import { ActionTree } from "vuex";
import { RootState } from "@/store/";
import { TodoState, Todo } from "./state";

 const todoActions: ActionTree<TodoState, RootState> = {
  createTodo({ commit }, todo: Todo) {
    return TodoService.postTodo(todo)
      .then(() => {
        commit('addTodo', todo);
      })
      .catch((error) => {
        console.log(error);
      });
  },

  fetchTodos({ commit }) {
    TodoService.getTodos().then((response) => {
      commit('setTodos', response.data);
    });
  },

  updateTodo({ commit, state, }, data: Todo) {
    const todoToBeUpdated = state.todos.find(
      (todo) => todo.id === data.id
    );
    
    if (!todoToBeUpdated) return;
    const newTodo = { ...todoToBeUpdated, done: !todoToBeUpdated.done,};
    const newTodos = [...state.todos.filter((todo) => todo.id !== data.id), newTodo];    
    commit('setTodos', newTodos);
  },
};

export default todoActions;