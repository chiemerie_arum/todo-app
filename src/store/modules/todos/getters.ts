import { GetterTree } from "vuex";
import { TodoState, Todo } from "./state";
import { RootState } from "@/store";

 const todoGetters: GetterTree<TodoState, RootState> = {
  pendingTodos(state): Array<Todo> {
    return state.todos.filter((todo) => todo.done === false);
  },

  doneTodos(state): Array<Todo> {
    return state.todos.filter((todo) => todo.done === true);
  },
};


export default todoGetters;